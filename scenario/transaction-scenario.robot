*** Settings ***
[Documentation]  Transaction scenario, validate UI and DB record
Library   Selenium2Library
Library   DatabaseLibrary
#         Uncomment library below if you are using headless browser
#Library   XvfbRobot
Suite Setup         Access Homepage
Suite Teardown      Close Browser

*** Variable ***
@{transact_config}       MySQLdb  transaction_transaction  db-user    db-user-pwd     10.10.10.10   3306
${site_homepage}         http://bukalapak.com/
${trx_path}              http://bukalapak.com/trx/detail/
${locator_id}            txt_id 
${locator_addr}          txt_address 
${locator_date}          txt_date  
${locator_seller}        txt_seller  
${locator_service}       txt_service

*** Test Case ***
Trans-001 - Validate Transaction
    [Documentation]  Verify data displayed on web referring to db
    ${transactions}  Get Transaction From transaction_transaction
    :FOR  ${transaction}  IN  @{transactions}
    \     Validate Displayed Transaction      ${transaction}

*** Keywords ***
Access Homepage
    [Documentation]  Open web browser, go to homepage
    # Uncomment code below if you are using headless browser
    # Start Virtual Display  1024  768  24
    Open Browser         ${site_homepage}

Get Transaction From transaction_transaction
    [Documentation]      Query transaction data from transaction_transaction
    ${query_string}      Catenate  Select TRX_ID, ADDRESS_SHIP, DATE_ORDER,
    ...                                   SELLER_NAME, DELIVERY_SERVICE
    ...                            From transaction_transaction
    Connect To Database  @{transact_config}[0]  @{transact_config}[1]  @{transact_config}[2]
    ...                  @{transact_config}[3]  @{transact_config}[4]  @{transact_config}[5]
    ${transactions}      Query     ${query_string}
    Disconnect From Database
    [Return]             ${transactions}

Validate Displayed Transaction
    [Documentation]      Verify whether displayed data is equal as saved data in db
    [Arguments]          ${transaction}
    Go To                ${trx_path}${transaction[0]}
    ${displayed_id}         Get Text  id=${locator_id}
    ${displayed_addr}       Get Text  id=${locator_addr}
    ${displayed_date}       Get Text  id=${locator_date}
    ${displayed_seller}     Get Text  id=${locator_seller}
    ${displayed_service}    Get Text  id=${locator_service}
    Should Be Equal         ${displayed_id}         ${transaction[0]}   msg=Invalid Transaction ID
    Should Be Equal         ${displayed_addr}       ${transaction[1]}   msg=Invalid Shipment Address
    Should Be Equal         ${displayed_date}       ${transaction[2]}   msg=Invalid Transaction Date
    Should Be Equal         ${displayed_seller}     ${transaction[3]}   msg=Invalid Seller Name
    Should Be Equal         ${displayed_service}    ${transaction[4]}   msg=Invalid Delivery Service
    Page Should Contain       Transaction Succesful!
    Page Should Contain Link  Main menu