This is an example of test automation for success transaction validation.
This project was developed based on the information given, and some assumptions of the developer about missing information.
Some of the assumptions used by the developer are:
1. Site home page and transaction url
2. Detail of the transaction can be accessed without login permission
3. All of the requirements needed to run robot script are installed and work properly
4. The number of data in database is not too large to validate (performance affect)

<h2>Test Scenario:</h2>
<h3>Test ID</h3>                                                  
Trans-001  

<h3>Scenario</h3>
Validate Transaction    

<h3>Steps</h3> 
1. Open db transaction_transaction       
2. Select one row data                                                                
3. Go to detail transaction on website                                                   
   http://bukalapak/trx/detail/TRX_ID                                             
4. Compare displayed data with saved data in db     

<h3>Expected Result</h3>
1. TRX_ID should be equal on database
2. Address shipment should be equal on database
3. Displayed Date order should be equal on database
4. Displayed Seller name should be equal on database
5. Displayed delivery service should be equal on database
6. There's should be text 'Transaction Succesful' text and 'Main menu' link